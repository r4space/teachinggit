# Branch and Merge

**Branching**
There are multiple possible workflows using git/version control, most make use of multiple branches.  Branches of a repository are parallel development sequences that can see multiple commits before rejoining (merging) with a central or main (often called 'master) branch

1. To get a new branch:
```bash
$ git branch bluefeature
```

2. To start working in that branch:
```bash
$ git checkout bluefeature
```

3. To see all available branches/see which branch you're currently on:
Not * next to current branch
```bash
$ git branch -a
```

4. To make a new branch and start working in it straight away:
```bash
$ git checkout -b redfeature
```

5. Merge branches:
This will bring the commit history and changes of the branch you name into whichever branch you're currently on when running the command.  This example merges whatever is in redfeature into master
```bash
$ git checkout master
$ git merge redfeature
```
When there is a merge conflict:
i] Manually:  Open conflicting files and fix issues
ii] git mergetool can be used but require more advanced knowledge of text editors so is not covered here

