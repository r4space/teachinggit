# Getting started with Git:

## 1. Installing basic tools:
**Two** tools are needed to follow the git lessons: (1) A Bash terminal interface and (2) Git the vesion control application.   The following contain instructions for installing both on the following operating systems:
* [Instructions for Windows](install_windows.md)
* [Instructions for MaxOS](install_macos.md)
* [Instructions for Linux](install.linux.md)

Once you've completed the instructions for your OS, you should be able to:
1. Open a terminal
2. In the terminal run:
```bash
$ git --version
```
You should see an output similar to the following (the actual version number may differ):
```bash
$ git --version
git version 2.27.0
```

## 2. Configuring Git locally for yourself:
**1. Locally:** Open a terminal and run the following commands to get Git setup for your use.

Obviously use your own name and email not Dracula's and if you would prefer to use a different text editor.  By configuring Git with these details it will mean every commit you make is tagged with your details so you can get the credit for work you've done.
```bash
$ git config --global user.name "Luke Skywalker"
$ git config --global user.email "Luke@skywalker.co.za"
$ git config --list
```

**2. Remotely:** Sign up for an account at [www.gitlab.com](https://gitlab.com/)

