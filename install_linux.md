# Installing tools for using git on Linux:

## Terminal (bash):
The default shell is usually Bash and there is usually no need to install anything.

To see if your default shell is Bash type echo $SHELL in a terminal and press the Enter key. If the message printed does not end with '/bash' then your default is something else and you can run Bash by typing bash.

## Git:
If Git is not already available on your machine you can try to install it via your distro's package manager. 
* For Debian/Ubuntu run:
```bash
$ sudo apt-get install git 
```
* For Fedora run 
```bash
$ sudo dnf install git
```


